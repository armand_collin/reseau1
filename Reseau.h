#ifndef RESEAU_H_INCLUDED
#define RESEAU_H_INCLUDED

#include <vector>
#include <random>
#include <tuple>
#include <iostream>
#include <string>
#include <fstream>
#include <math.h>

using namespace std;


struct GradientData
{
   vector<vector<double>> gradSeuils;
   vector<vector<vector<double>>> gradPoids;
};

class Reseau
{
public:
   Reseau(int tailleX, int largeur, int tailleY);

   // fonctions du réseau
   vector<double> propagationAvant(vector<double> x);
   vector<double> entrainementDGS(vector<vector<double>> x, vector<double> y, int iterations,
                  int tailleMiniLot, double eta, vector<vector<double>> xTest, vector<double> yTest_, string outFilename);
   void           cycleApprentissage(vector<vector<double>> miniLot, vector<double> miniLotY, double eta);
   GradientData   retropropagation(vector<double> x, double y);
   void           actualiserParametres(GradientData grad, double eta, int n);
   vector<double> evaluer(vector<vector<double>> xTest, vector<double> yTest);

   // opérations
   vector<double>          addVec(vector<double> a, vector<double> b);
   vector<double>          subVec(vector<double> a, vector<double> b);
   vector<double>          mulVec(double k, vector<double> vec);
   double                  maxVec(vector<double> vec);
   vector<vector<double>>  prodVec(vector<double> a, vector<double> b);
   vector<vector<double>>  addMat(vector<vector<double>> a, vector<vector<double>> b);
   vector<double>          mulMat(vector<vector<double>> a, vector<double> b);
   vector<vector<double>>  transposition(vector<vector<double>> a);
   vector<double>          sigmoide(vector<double> z);
   vector<double>          sigmoidePrime(vector<double> z);
   double                  entropieCroisee(vector<double> y, vector<double> yDes);
   vector<double>          entropieCroiseePrime(vector<double> activations, vector<double> yDes);
   vector<double>          vectoriser(double c);
   void                    sommateurGradientData(GradientData* dst, GradientData src);
   void                    initGradientData(GradientData* grad);

private:
   int tailles_[3];
   vector<double> seuilsCC_;
   vector<double> seuilsCS_;
   vector<vector<double>> poidsCC_;
   vector<vector<double>> poidsCS_;
};


#endif
