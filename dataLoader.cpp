/**
* Fonctions pour lire et enregistrer les donnees MNIST de chiffres manuscrits
* \file     dataLoader.cpp
* \author   Armand COLLIN
* \date     27 Septembre 2019
**/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "dataLoader.h"

using namespace std;

int inversionInt(int i)
{
   unsigned char o1, o2, o3, o4;
   int inversion = 0;
   o1 = i & 0xFF;
   inversion += (int) o1 << 24;
   o2 = (i >> 8) & 0xFF;
   inversion += (int) o2 << 16;
   o3 = (i >> 16) & 0xFF;
   inversion += (int) o3 << 8;
   o4 = (i >> 24) & 0xFF;
   inversion += (int) o4;
   return inversion;
}

void lireLabels(string nomFichier, LabelData &data)
{
   ifstream fichierLabel;
   fichierLabel.open(nomFichier, ios::binary);
   if (fichierLabel.is_open())
   {
      int int1 = 0;
      int int2 = 0;
      fichierLabel.read((char*) &int1, sizeof(int1)); // lecture nb magique
      fichierLabel.read((char*) &int2, sizeof(int2)); // lecture nb items
      data.magicNumber = inversionInt(int1);
      data.nItems = inversionInt(int2);
      // lecture des labels:
      for (int i = 0; i < data.nItems; i++)
      {
         unsigned char temp = 0;
         fichierLabel.read((char*) &temp, sizeof(temp));
         data.labels.push_back((double) temp);
      }

      fichierLabel.close();
   }
   else
      cout << "Erreur d'ouverture du fichier." << endl;
   cout << data.nItems << " labels lus." << endl;
}

void lireImages(string nomFichier, ImageData &data)
{
   ifstream fichier;
   int      int1 = 0, int2 = 0, int3 = 0, int4 = 0;

   fichier.open(nomFichier, ios::binary);
   if (fichier.is_open())
   {
      fichier.read((char*) &int1, sizeof(int1));
      fichier.read((char*) &int2, sizeof(int2));
      fichier.read((char*) &int3, sizeof(int3));
      fichier.read((char*) &int4, sizeof(int4));
      data.magicNumber = inversionInt(int1);
      data.nItems = inversionInt(int2);
      data.nRows = inversionInt(int3);
      data.nCols = inversionInt(int4);

      // lecture des images
      for (int i = 0; i < data.nItems; i++)
      {
         vector<double> img;
         for (int j = 0; j < data.nRows; j++)
         {
            for (int k = 0; k < data.nCols; k++)
            {
               unsigned char pixel = 0;
               fichier.read((char*) &pixel, sizeof(pixel));
               // pas de nuance de gris:
               if (pixel == 0)
                  img.push_back((double) pixel);
               else
                  img.push_back(1.0);
            }
         }
         data.images.push_back(img);
      }
      cout << data.nItems << " images lues." << endl;

   }
}

/*
int main()
{

   LabelData labelDataTest;
   ImageData imageDataTest;
   lireLabels("t10k-labels-idx1-ubyte", labelDataTest);
   lireImages("t10k-images-idx3-ubyte", imageDataTest);

}
*/
