#include "dataLoader.h"
#include "Reseau.h"
#include <iostream>
#include <fstream>

using namespace std;

int main()
{

   LabelData yTest, y;
   ImageData xTest, x;
   vector<vector<double>> xVal;
   vector<double> yVal;
   lireLabels("data_MNIST/t10k-labels-idx1-ubyte", yTest);
   lireLabels("data_MNIST/train-labels-idx1-ubyte", y);
   lireImages("data_MNIST/t10k-images-idx3-ubyte", xTest);
   lireImages("data_MNIST/train-images-idx3-ubyte", x);
   for(int i = x.images.size()-1; i >= 55000; i--)
   {
      xVal.push_back( x.images[i] );
      yVal.push_back( y.labels[i] );
      x.images.pop_back();
      y.labels.pop_back();
   }

   Reseau rez = Reseau(784, 30, 10);
   double eta = 6.5;
   int it = 137;
   int size = 400;
   string nom = "resultatsValidation/30neurones16";
   cout<<"ETA: "<<eta<<endl<<"EPOCHS: "<<it<<endl<<"MINILOT: "<<size<<endl;
   rez.entrainementDGS( x.images, y.labels, it, size, eta, xVal, yVal, nom );

   auto eval = rez.evaluer(xTest.images, yTest.labels);
   cout<<"EVALUATION ENSEMBLE TEST:"<<endl<<"\tCOST: "<<eval[0]<<endl
                                 <<"\tACCURACY: "<<eval[1]*100<<endl;

}
