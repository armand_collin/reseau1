#ifndef DATALOADER_H_INCLUDED
#define DATALOADER_H_INCLUDED


// dependances
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

// structures de donnees
struct LabelData
{
   int magicNumber;
   int nItems;
   std::vector<double> labels;
};

struct ImageData
{
   int magicNumber;
   int nItems;
   int nRows;
   int nCols;
   std::vector<std::vector<double>> images;
};

// fonctions
int inversionInt(int i);
void lireLabels(std::string nomFichier, LabelData &data);
void lireImages(std::string nomFichier, ImageData &data);

#endif
