/**
* Réseau neuronal :
*  -entièrement connecté
*  -1 couche cachée
*  -activation : sigmoide
*  -fonction de coût: entropie croisée
* \file     Reseau.cpp
* \author   Armand COLLIN
* \date     7 octobre 2019
**/

#include "Reseau.h"
using namespace std;

Reseau::Reseau(int tailleX, int largeur, int tailleY)
   : tailles_({tailleX, largeur, tailleY})
{
   // initialisation des poids/seuils avec une loi normale centrée réduite
   random_device  rd;
   mt19937        gen(rd());
   normal_distribution<float> distribution(0.0, 0.1);
   // couche cachée
   for(int i=0; i<tailles_[1]; i++)
   {
      vector<double> poidsNeurone;
      for(int j=0; j<tailles_[0]; j++)
      {
         poidsNeurone.push_back( distribution(gen) );
      }
      seuilsCC_.push_back( distribution(gen) );
      poidsCC_.push_back(poidsNeurone);
   }
   // couche sortie
   for(int i=0; i<tailles_[2]; i++)
   {
      vector<double> poidsNeurone;
      for(int j=0; j<tailles_[1]; j++)
      {
         poidsNeurone.push_back( distribution(gen) );
      }
      seuilsCS_.push_back( distribution(gen) );
      poidsCS_.push_back(poidsNeurone);
   }
}

// Retourne le vecteur de sortie étant donné l'entrée x
vector<double> Reseau::propagationAvant(vector<double> x)
{
   vector<double> z1,h1,z2,h2;
   // propagation à travers la couche cachée
   z1 = addVec( seuilsCC_, mulMat(poidsCC_, x) );
   h1 = sigmoide(z1);
   // propagation à travers la couche de sortie
   z2 = addVec( seuilsCS_, mulMat(poidsCS_, h1) );
   h2 = sigmoide(z2);

   return h2;
}

/**
 * Applique une descente de gradient stochastique sur l'ensemble d'entrainement
 *
 * @param X             ensemble d'entrainement
 * @param y             labels associés aux exemples
 * @param iterations    nb de cycles d'apprentissage
 * @param tailleMiniLot taille des mini-lots d'entrainement à chaque itération
 * @param eta           taux d'apprentissage
 * @return cout         vecteur contenant le cout par itération
 */
vector<double> Reseau::entrainementDGS(vector<vector<double>> x,
                                       vector<double> y, int iterations,
                                       int tailleMiniLot, double eta,
                                       vector<vector<double>> xTest,
                                       vector<double> yTest, string outFilename)
{
   //tuple< vector<vector<double>>, vector<double> > lot;
   //tuple< vector<vector<double>>, vector<double> > miniLot;
   //lot = make_tuple(x,y);
   vector<double> vecteurCout;
   if (x.size() < iterations*tailleMiniLot)
   {
      cout << "ERREUR: donnees insuffisantes" << endl;
      return vecteurCout;
   }

   ofstream fichierSave;
   fichierSave.open(outFilename, ios::ate);
   fichierSave <<"ETA: "<<eta<<endl<<"EPOCHS: "<<iterations<<endl<<"MINILOT: "
               <<tailleMiniLot<<endl<<endl;
   bool once = false;
   double etaTemp;

   for (int i = 0; i < iterations; i++)
   {
      if (i==0)
         etaTemp = 1.5;
      else if (i==1)
         etaTemp = eta;
      vector<vector<double>> miniLot;
      vector<double> miniLotY;
      // construction du mini-lot
      for (int j = 0; j < tailleMiniLot; j++)
      {
         int index = i*tailleMiniLot + j;
         miniLot.push_back( x[index] );
         miniLotY.push_back( y[index] );
      }
      cycleApprentissage(miniLot, miniLotY, etaTemp);
      auto eval = evaluer(xTest, yTest);
      fichierSave <<"ITERATION\t"<<i<<"\t"<<eval[0]<<"\tprecision\t"
                  <<eval[1]*100<<endl;
      cout  <<"ITERATION "<<i<<"\t: "<<eval[0]<<"\tprecision: "
            <<eval[1]*100<<"%"<<endl;

      // diminution du taux d'apprentissage
      // if ( (eval[0] < 1.5) && !once )
      // {
      //    etaTemp /= 2.0;
      //    cout<<"ETA: "<<etaTemp<<endl;
      //    fichierSave<<"ETA: "<<eta<<endl;
      //    once = true;
      // }
   }
   fichierSave.close();

   return vecteurCout;
}

/**
 * Effectue un cycle d'apprentissage par DGS sur le mini-lot
 *
 * @param miniLot sous-ensemble d'apprentissage
 * @param eta     taux d'apprentissage
 * @return        rien pour l'instant
 */
void Reseau::cycleApprentissage( vector<vector<double>> miniLot,
                                 vector<double> miniLotY,
                                 double eta)
{
   GradientData gradients;
   initGradientData(&gradients);
   for(int i = 0; i < miniLot.size(); i++)
   {
      auto x = miniLot[i];
      auto y = miniLotY[i];
      GradientData gradientsPartiels = retropropagation(x,y);
      sommateurGradientData(&gradients, gradientsPartiels);
   }
   actualiserParametres(gradients, eta, miniLot.size());

}

/**
 * Applique la rétropropagation sur l'exemple fourni
 *
 * @param x exemple d'apprentissage
 * @param y label de l'exemple
 * @return  struct GradientData contenant tous les gradients
 */
GradientData Reseau::retropropagation(vector<double> x, double y)
{
   GradientData gradients;

   // activation premiere couche
   // calcul des activations
   //TODO: GENERALISER POUR L COUCHES, store les activations (x,z1,h1,h2, ...)
   vector<double> z1,h1,z2,h2;
   z1 = addVec( seuilsCC_, mulMat(poidsCC_, x) );
   h1 = sigmoide(z1);
   z2 = addVec( seuilsCS_, mulMat(poidsCS_, h1) );
   h2 = sigmoide(z2);

   // gradients derniere couche
   vector<double> delta2 = entropieCroiseePrime(h2, vectoriser(y));
   vector<double> sigPrime = sigmoidePrime(z2);
   for(int i = 0; i < h2.size(); i++)
      delta2[i] = delta2[i]*sigPrime[i];
   gradients.gradSeuils.insert(gradients.gradSeuils.begin(), delta2);
   vector<vector<double>> gradPoids = prodVec(delta2, h1);
   gradients.gradPoids.insert(gradients.gradPoids.begin(), gradPoids);

   vector<vector<double>> poidsCST = transposition(poidsCS_);
   vector<double> delta1 = mulMat(poidsCST, delta2);
   sigPrime = sigmoidePrime(z1);
   for(int i = 0; i < delta1.size(); i++)
      delta1[i] = delta1[i]*sigPrime[i];
   gradients.gradSeuils.insert(gradients.gradSeuils.begin(), delta1);
   gradPoids = prodVec(delta1, x);
   gradients.gradPoids.insert(gradients.gradPoids.begin(), gradPoids);

   return gradients;
}

/**
 *
 *
 * @param grad struct contenant tous les gradients
 * @param eta  taux d'apprentissage
 * @param n    taille du mini-lot
 * @return     rien
 */
void Reseau::actualiserParametres(GradientData grad, double eta, int n)
{
   //TODO: generaliser pour L couches

   double k = eta/n;
   // actualisation couche cachée
   for(int i = 0; i < seuilsCC_.size(); i++)
   {
      double b = seuilsCC_[i];
      double bGrad = grad.gradSeuils[0][i];
      seuilsCC_[i] = b - k*bGrad;

      vector<double> w = poidsCC_[i];
      vector<double> wGrad = grad.gradPoids[0][i];
      poidsCC_[i] = subVec( w, mulVec(k, wGrad) );

   }
   // actualisation couche sortie
   for(int i = 0; i < seuilsCS_.size(); i++)
   {
      double b = seuilsCS_[i];
      double bGrad = grad.gradSeuils[1][i];
      seuilsCS_[i] = b - k*bGrad;

      vector<double> w = poidsCS_[i];
      vector<double> wGrad = grad.gradPoids[1][i];
      poidsCS_[i] = subVec( w, mulVec(k, wGrad) );
   }
}

/**
 * Calcule le coût
 *
 * @param xTest   données de test
 * @param yTest   labels des données de test
 * @return cost   somme pondérée du cout de chaque exemple du jeu de données
 */
vector<double> Reseau::evaluer(  vector<vector<double>> xTest,
                                 vector<double> yTest)
{
   double cost = 0.0;
   double precision = 0.0;
   for(int i = 0; i < xTest.size(); i++)
   {
      vector<double> sortie = propagationAvant( xTest[i] );
      if (maxVec(sortie) == yTest[i])
         precision += 1;
      cost += entropieCroisee( sortie, vectoriser(yTest[i]) );
   }
   cost /= yTest.size();
   precision /= yTest.size();
   vector<double> eval = {cost, precision};

   return eval;
}

/**
 * Effectue la somme de deux struct GradientData élément par élément
 *
 * @param dst  ptr sur opérande et destination de la somme
 * @param src  seconde opérande
 * @return     rien
 */
void Reseau::sommateurGradientData(GradientData* dst, GradientData src)
{
   for(int i = 0; i < src.gradSeuils.size(); i++)
   {
      vector<double> op1 = dst->gradSeuils[i];
      vector<double> op2 = src.gradSeuils[i];
      dst->gradSeuils[i] = addVec(op1, op2);
      auto mat1 = dst->gradPoids[i];
      auto mat2 = src.gradPoids[i];
      dst->gradPoids[i] = addMat(mat1, mat2);
   }

}

/**
 * Initialise la struct GradientData avec les bonnes dimensions
 *
 * @param grad struct vide à initialiser
 * @return     rien
 */
void Reseau::initGradientData(GradientData* grad)
{
   double init = 0.0;

   // initialisation couche cachée
   vector<double> b;
   vector<vector<double>> w;
   for (int i = 0; i < tailles_[1]; i++)
   {
      b.push_back( init );
      vector<double> w_i;
      for (int j = 0; j < tailles_[0]; j++)
         w_i.push_back( init );
      w.push_back( w_i );
   }
   grad->gradSeuils.push_back( b );
   grad->gradPoids.push_back( w );

   // initialisation couche de sortie
   b.clear();
   w.clear();
   for (int i = 0; i < tailles_[2]; i++)
   {
      b.push_back( init );
      vector<double> w_i;
      for (int j = 0; j < tailles_[1]; j++)
         w_i.push_back( init );
      w.push_back( w_i );
   }
   grad->gradSeuils.push_back( b );
   grad->gradPoids.push_back( w );

}

vector<double> Reseau::addVec(vector<double> a, vector<double> b)
{
   vector<double> somme;

   for(int i = 0; i < a.size(); i++)
   {
      somme.push_back( a[i] + b[i] );
   }
   return somme;
}

vector<double> Reseau::subVec(vector<double> a, vector<double> b)
{
   vector<double> diff;

   for(int i = 0; i < a.size(); i++)
   {
      diff.push_back( a[i] - b[i] );
   }
   return diff;
}

// Effectue la multiplication du scalaire k avec le vecteur vec
vector<double> Reseau::mulVec(double k, vector<double> vec)
{
   for (int i = 0; i < vec.size(); i++)
      vec[i] *= k;
   return vec;
}

// Retourne la valeur maximale contenue dans le vecteur
double Reseau::maxVec(vector<double> vec)
{
   int max = 0;
   for(int i = 0; i < vec.size(); i++)
   {
      if (vec[i] >= vec[max])
         max = i;
   }
   return (double) max;
}

vector<vector<double>> Reseau::prodVec(vector<double> a, vector<double> b)
{
   vector<vector<double>> resultat;
   for(int i = 0; i < a.size(); i++)
   {
      vector<double> col;
      for(int j = 0; j < b.size(); j++)
      {
         col.push_back( a[i]*b[j] );
      }
      resultat.push_back( col );
   }
   return resultat;
}

vector<vector<double>> Reseau::addMat( vector<vector<double>> a,
                                       vector<vector<double>> b)
{
   vector<vector<double>> resultat;
   for(int i = 0; i < a.size(); i++)
   {
      vector<double> op1 = a[i];
      vector<double> op2 = b[i];
      resultat.push_back( addVec(op1, op2) );
   }
   return resultat;
}

/*
Effectue une multiplication matricielle entre une matrice a (mxn) et b (nx1) et
retourne un vecteur c (mx1)
*/
vector<double> Reseau::mulMat(vector<vector<double>> a, vector<double> b)
{
   vector<double> c;
   // verification taille
   if ( a[0].size() != b.size() )
   {
      cout << "ERREUR: dimension incompatible pour mulMat()" << endl;
      return c;
   }
   else
   {
      for(int i = 0; i < a.size(); i++)
      {
         double elem = 0.0;
         for(int k = 0; k < a[0].size(); k++)
         {
            elem += a[i][k] * b[k];
         }
         c.push_back(elem);
      }
      return c;
   }

}

// Effectue une transposition matricielle
vector<vector<double>> Reseau::transposition(vector<vector<double>> a)
{
   vector<vector<double>> resultat;
   for(int c = 0; c < a[0].size(); c++)
   {
      vector<double> ligne;
      for(int l = 0; l < a.size(); l++)
      {
         ligne.push_back( a[l][c] );
      }
      resultat.push_back( ligne );
   }
   return resultat;
}

// Applique élément par élément la fonction sigmoïde à l'argument
vector<double> Reseau::sigmoide(vector<double> z)
{
   vector<double> out;
   for(int i=0; i < z.size(); i++)
   {
      out.push_back( 1.0/(1.0 + exp(-1*z[i]) ) );
   }
   return out;
}

// Applique élément par élément la dérivée de la fonction sigmoïde à l'argument
vector<double> Reseau::sigmoidePrime(vector<double> z)
{
   vector<double> out;
   vector<double> sig = sigmoide(z);
   for(int i=0; i < z.size(); i++)
   {
      out.push_back( sig[i]*(1-sig[i]) );
   }
   return out;
}

// Calcule l'entropie croisée entre les prédictions et la sortie désirée
double Reseau::entropieCroisee(vector<double> y, vector<double> yDes)
{
   double somme = 0.0;
   for(int i = 0; i < y.size(); i++)
   {
      somme += (-1*yDes[i]*log(y[i])  -  (1-yDes[i])*log(1-y[i]) );
   }
   return somme;
}

// Calcule les dérivées partielles de la fonction d'entropie croisée
vector<double> Reseau::entropieCroiseePrime( vector<double> activations,
                                             vector<double> yDes)
{
   return subVec(activations, yDes);
}

// Vectorise le chiffre spécifié
vector<double> Reseau::vectoriser(double c)
{
   vector<double> vec;
   for(double i = 0; i < 10; i++)
   {
      if (i == c)
         vec.push_back(1);
      else
         vec.push_back(0);
   }
   return vec;
}
